package com.qa.box.hybrid.executionEngine;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class readPDFTestSelenium {
	
	
	@Test
	public void readPDFTest() throws IOException {
		
		System.setProperty("webdriver.chrome.driver", "C:/Ravi_Automation/KeywordDrivenBoxxPort/KeywordDrivenBoxxPort/Driver/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.get("http://www.africau.edu/images/default/sample.pdf");
		
		//driver.get("file:///C:/Users/rkuma115/Downloads/sample.pdf");
		
		

		String CurrentUrl =driver.getCurrentUrl();
		System.out.println(CurrentUrl);
		
		URL url = new URL(CurrentUrl);
		
		//URL url = new URL("file:///C:/Users/rkuma115/Downloads/sample.pdf");
		
		InputStream ls = url.openStream();
		
		BufferedInputStream fileparse = new BufferedInputStream(ls);
		
		
		PDDocument document  = null;
		document = PDDocument.load(fileparse);
		
		 String pdfContent = new PDFTextStripper().getText(document);
		
		System.out.println(pdfContent);
		
		
		Assert.assertTrue(pdfContent.contains("A Simple PDF File"));
		Assert.assertTrue(pdfContent.contains("Simple PDF File 2"));
		Assert.assertTrue(pdfContent.contains("...continued from page 1. Yet more text. And more text. And more text."));
		
		
		/*
		 * Assert.assertTrue(pdfContent.
		 * contains("...continued from page 1. Yet more text. And more text. And more text."
		 * )); System.out.println("Printing PDF Content" +pdfContent);
		 */
		
		
	}

}
